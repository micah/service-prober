package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"regexp"
	"syscall"

	"git.autistici.org/ai3/tools/service-prober/probes"
	"git.autistici.org/ai3/tools/service-prober/scheduler"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	_ "git.autistici.org/ai3/tools/service-prober/probes/http"
	_ "git.autistici.org/ai3/tools/service-prober/probes/imap"
)

var (
	config      = flag.String("config", "", "configuration file `path`")
	oneshot     = flag.Bool("oneshot", false, "run just one shot and exit")
	validate    = flag.Bool("validate", false, "just validate the configuration syntax")
	only        = flag.String("only", "", "only run probes with names matching this `regex`")
	httpAddr    = flag.String("http-addr", ":5522", "`address` for the HTTP server")
	keepResults = flag.Int("keep-results", 100, "keep `N` results of past probes in memory")
)

func filterProbes(pp []*probes.Probe) []*probes.Probe {
	rx, err := regexp.Compile(*only)
	if err != nil {
		log.Fatalf("error in --only: %v", err)
	}

	out := pp[:0]
	for _, p := range pp {
		if rx.MatchString(p.Name()) {
			out = append(out, p)
		}
	}
	return out
}

func main() {
	log.SetFlags(0)
	flag.Parse()
	if flag.NArg() > 0 {
		log.Fatal("Too many arguments, run with --help for details.")
	}
	if *config == "" {
		log.Fatal("Must specify --config")
	}

	cfgdata, err := ioutil.ReadFile(*config)
	if err != nil {
		log.Fatal(err)
	}

	var store probes.ResultStore
	if *oneshot {
		store = &dumpResultStore{}
	} else {
		store = probes.NewInstrumentedResultStore(
			probes.NewResultStore(*keepResults, *keepResults))
	}

	probes, err := probes.ParseConfig(cfgdata, store)
	if err != nil {
		log.Fatal(err)
	}

	// Exit now if we're only validating the configuration.
	if *validate {
		log.Printf("configuration is ok (%d probes defined)", len(probes))
		os.Exit(0)
	}

	probes = filterProbes(probes)

	// Create a controlling Context that we'll cancel to terminate
	// cleanly on signals.
	ctx, cancel := context.WithCancel(context.Background())
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	// With --one-shot, run all probes once and exit.
	if *oneshot {
		for _, p := range probes {
			p.Tick(ctx)
		}
		os.Exit(0)
	}

	// Create and start the HTTP server, for debug pages
	// and Prometheus metrics.
	httpServer := &debugServer{store: store}
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/detail", httpServer.handleDebugDetails)
	http.HandleFunc("/", httpServer.handleDebugHome)
	go func() {
		log.Fatal(http.ListenAndServe(*httpAddr, nil))
	}()

	// Create and run the scheduler, which is the 'controlling'
	// task - we're going to add our tasks in the background.
	sched := scheduler.New()

	go func() {
		for _, p := range probes {
			sched.Add(p)
		}
		log.Printf("scheduled %d probes", len(probes))
	}()

	log.Printf("starting service-prober")
	sched.Run(ctx)
	log.Printf("terminating service-prober")
}
