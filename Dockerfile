FROM golang:1.16 as build
ADD . /src
RUN apt-get update && apt-get install --no-install-recommends -y libcap2-bin
RUN cd /src && go build -tags netgo -o service-prober ./cmd/service-prober && strip service-prober

FROM scratch
COPY --from=build /src/service-prober /service-prober
ENTRYPOINT ["/service-prober"]

