package dns

import (
	"context"
	"crypto/tls"
	"net"
)

// A net.Dialer equivalent, with the capability of overriding DNS lookup results.
type DNSOverrideDialer struct {
	*net.Dialer
	dns       map[string]string
	tlsConfig *tls.Config
}

func NewDNSOverrideDialer(dnsMap map[string]string, tlsConfig *tls.Config) *DNSOverrideDialer {
	return &DNSOverrideDialer{
		Dialer:    new(net.Dialer),
		dns:       dnsMap,
		tlsConfig: tlsConfig,
	}
}

func (d *DNSOverrideDialer) resolve(addr string) (string, string, error) {
	host, port, err := net.SplitHostPort(addr)
	if err != nil {
		return "", "", err
	}
	if override, ok := d.dns[host]; ok {
		addr = net.JoinHostPort(override, port)
	}
	return addr, host, nil
}

func (d *DNSOverrideDialer) DialContext(ctx context.Context, network, addr string) (net.Conn, error) {
	resolved, _, err := d.resolve(addr)
	if err != nil {
		return nil, err
	}
	return d.Dialer.DialContext(ctx, network, resolved)
}

func (d *DNSOverrideDialer) DialTLSContext(ctx context.Context, network, addr string) (net.Conn, error) {
	resolved, servername, err := d.resolve(addr)
	if err != nil {
		return nil, err
	}
	config := d.tlsConfig
	if config == nil {
		config = new(tls.Config)
	}
	if config.ServerName == "" {
		config = config.Clone()
		config.ServerName = servername
	}
	return (&tls.Dialer{Config: config}).DialContext(ctx, network, resolved)
}
