package probes

import "github.com/prometheus/client_golang/prometheus"

var (
	probeSuccess = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "probe_success",
			Help: "Success of a specific probe.",
		},
		[]string{"probe", "probeset"},
	)

	probeLastRun = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "probe_last_run_ts",
			Help: "Last timestamp of a probe execution, regardless of result.",
		},
		[]string{"probe", "probeset"},
	)
)

func init() {
	prometheus.MustRegister(probeSuccess, probeLastRun)
}

type promResultStore struct {
	ResultStore
}

func NewInstrumentedResultStore(wrap ResultStore) ResultStore {
	return &promResultStore{wrap}
}

func (p *promResultStore) Push(result *Result) {
	p.ResultStore.Push(result)

	var success float64
	if result.Ok {
		success = 1
	}

	probeset := result.Spec.Labels["probeset"]
	probeSuccess.WithLabelValues(result.Spec.Name, probeset).Set(success)
	probeLastRun.WithLabelValues(result.Spec.Name, probeset).Set(float64(result.Start.Unix()))
}
