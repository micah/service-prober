package imap

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/mail"
	"net/textproto"
	"strings"
	"time"

	"git.autistici.org/ai3/tools/service-prober/common/jsontypes"
	"git.autistici.org/ai3/tools/service-prober/common/vars"
	"git.autistici.org/ai3/tools/service-prober/probes"
	"git.autistici.org/ai3/tools/service-prober/protocol/imap"
	"git.autistici.org/ai3/tools/service-prober/protocol/smtp"
	"git.autistici.org/ai3/tools/service-prober/protocol/ssl"
)

var defaultCheckInterval = 5 * time.Second

type imapRoundtripProbeSpec struct {
	DNSMap      map[string]string `json:"dns_map"`
	MessageBody string            `json:"message_body"`
	IMAP        struct {
		Addr     string          `json:"addr"`
		SSLOpts  *ssl.SSLOptions `json:"ssl"`
		Username string          `json:"username"`
		Password string          `json:"password"`

		ExpectedFolder  string                      `json:"expected_folder"`
		ExpectedHeaders map[string]jsontypes.Regexp `json:"expected_headers"`
	} `json:"imap"`
	SMTP struct {
		Addr     string          `json:"addr"`
		SSLOpts  *ssl.SSLOptions `json:"ssl"`
		Username string          `json:"username"`
		Password string          `json:"password"`
	} `json:"smtp"`
	CheckInterval jsontypes.Duration `json:"check_interval"`
}

func parseIMAPRoundtripProbeSpec(params json.RawMessage) (probes.Spec, error) {
	var spec imapRoundtripProbeSpec
	err := json.Unmarshal(params, &spec)
	return &spec, err
}

func (spec *imapRoundtripProbeSpec) Build(lookup map[string]interface{}) (probes.ProbeImpl, error) {
	expanded, err := vars.Expand(spec, lookup)
	if err != nil {
		return nil, err
	}
	s := expanded.(*imapRoundtripProbeSpec)

	// Sanity checks, and set defaults.
	if s.IMAP.Addr == "" {
		return nil, errors.New("imap.addr is unset")
	}
	if s.IMAP.Username == "" {
		return nil, errors.New("imap.username is unset")
	}
	if s.SMTP.Addr == "" {
		return nil, errors.New("smtp.addr is unset")
	}
	if s.SMTP.Username == "" {
		return nil, errors.New("smtp.username is unset")
	}
	if s.CheckInterval.Duration == 0 {
		s.CheckInterval = jsontypes.Duration{defaultCheckInterval}
	}
	if s.IMAP.ExpectedFolder == "" {
		s.IMAP.ExpectedFolder = "INBOX"
	}

	// Parse SSL options.
	imapSSLOpts, err := ssl.ParseSSLOptions(s.IMAP.SSLOpts)
	if err != nil {
		return nil, err
	}
	smtpSSLOpts, err := ssl.ParseSSLOptions(s.SMTP.SSLOpts)
	if err != nil {
		return nil, err
	}

	return &imapRoundtripProbe{
		from:          s.SMTP.Username,
		to:            s.IMAP.Username,
		msgBody:       s.MessageBody,
		imapAddr:      s.IMAP.Addr,
		imapUsername:  s.IMAP.Username,
		imapPassword:  s.IMAP.Password,
		imapTLS:       imapSSLOpts,
		imapHeaders:   s.IMAP.ExpectedHeaders,
		imapFolder:    s.IMAP.ExpectedFolder,
		smtpAddr:      s.SMTP.Addr,
		smtpUsername:  s.SMTP.Username,
		smtpPassword:  s.SMTP.Password,
		smtpTLS:       smtpSSLOpts,
		dnsMap:        s.DNSMap,
		checkInterval: s.CheckInterval.Duration,
	}, nil
}

type imapRoundtripProbe struct {
	from, to      string
	msgBody       string
	imapAddr      string
	imapUsername  string
	imapPassword  string
	imapTLS       *tls.Config
	imapHeaders   map[string]jsontypes.Regexp
	imapFolder    string
	smtpAddr      string
	smtpUsername  string
	smtpPassword  string
	smtpTLS       *tls.Config
	checkInterval time.Duration
	dnsMap        map[string]string
}

func (p *imapRoundtripProbe) sendMessage(ctx context.Context, debug *log.Logger) (string, error) {
	msgid, msg := smtp.NewTestMessage(p.from, p.to, p.msgBody)

	debug.Printf("making SMTP connection to %s", p.smtpAddr)
	conn, err := smtp.Dial(ctx, p.smtpAddr, p.smtpTLS, debug, p.dnsMap)
	if err != nil {
		return "", err
	}
	defer conn.Close()

	debug.Printf("sending test email from %s to %s (msgid %s)", p.from, p.to, msgid)
	err = conn.SendMail(p.smtpUsername, p.smtpPassword, p.from, []string{p.to}, msg)
	return msgid, err
}

func (p *imapRoundtripProbe) checkForMessage(ctx context.Context, debug *log.Logger, msgid string, fetchBody bool) (bool, string, error) {
	debug.Printf("making IMAP connection to %s", p.imapAddr)
	conn, err := imap.Dial(ctx, p.imapAddr, p.imapTLS, debug, p.dnsMap)
	if err != nil {
		return false, "", err
	}
	defer conn.Close()

	debug.Printf("logging in as %s", p.imapUsername)
	if err := conn.LoginSASL(p.imapUsername, p.imapPassword); err != nil {
		return false, "", err
	}

	debug.Printf("opening folder %s", p.imapFolder)
	if err := conn.Select(p.imapFolder); err != nil {
		return false, "", err
	}

	msgs, err := conn.Search(fmt.Sprintf("HEADER Message-ID %s", msgid))
	if err != nil {
		return false, "", err
	}

	if len(msgs) == 0 {
		debug.Printf("message %s not found yet", msgid)
		return false, "", nil
	}
	debug.Printf("message %s found!", msgid)

	var body string
	if fetchBody {
		body, err = conn.Fetch(msgs[0])
		if err != nil {
			return true, "", err
		}
	}

	debug.Printf("deleting test message %s", msgs[0])
	if err := conn.Store(msgs[0], imap.IMAPFlagDeleted); err != nil {
		return true, body, err
	}

	return true, body, conn.Expunge()
}

func (p *imapRoundtripProbe) waitForMessage(ctx context.Context, debug *log.Logger, msgid string) error {
	t := time.NewTicker(p.checkInterval)
	defer t.Stop()
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-t.C:
			ok, body, err := p.checkForMessage(ctx, debug, msgid, p.imapHeaders != nil)
			if err != nil {
				return err
			}
			if ok {
				if p.imapHeaders != nil {
					return p.checkHeaders(body, debug)
				}
				return nil
			}
		}
	}
}

func (p *imapRoundtripProbe) checkHeaders(body string, debug *log.Logger) error {
	msg, err := mail.ReadMessage(strings.NewReader(body))
	if err != nil {
		return err
	}

	for hdr, rx := range p.imapHeaders {
		hdr = textproto.CanonicalMIMEHeaderKey(hdr)
		found := false
		for _, value := range msg.Header[hdr] {
			if rx.MatchString(value) {
				debug.Printf("message header %s matches regexp %s: '%s'", hdr, rx, value)
				found = true
				break
			}
		}
		if !found {
			return fmt.Errorf("message header %s does not match expected regexp %s: '%s'", hdr, rx, msg.Header.Get(hdr))
		}
	}
	return nil
}

func (p *imapRoundtripProbe) RunProbe(ctx context.Context, debug *log.Logger) error {
	msgid, err := p.sendMessage(ctx, debug)
	if err != nil {
		return err
	}
	return p.waitForMessage(ctx, debug, msgid)
}

func init() {
	probes.RegisterProbeType("imap_roundtrip", parseIMAPRoundtripProbeSpec)
}
