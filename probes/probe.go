package probes

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"time"

	"git.autistici.org/ai3/tools/service-prober/common/jsontypes"
	"git.autistici.org/ai3/tools/service-prober/common/vars"
)

var (
	defaultInterval = 5 * time.Minute
	defaultTimeout  = 5 * time.Minute
)

// SpecCommon holds the parameters common to all probe specifications.
type SpecCommon struct {
	Type     string             `json:"type"`
	Name     string             `json:"name"`
	Loop     []string           `json:"loop"`
	Labels   map[string]string  `json:"labels"`
	Interval jsontypes.Duration `json:"interval"`
	Timeout  jsontypes.Duration `json:"timeout"`
}

// Type used to deserialize the common part of a Probe specification,
// and defer 'params' unmarshaling to a type-specific constructor.
type rawSpec struct {
	SpecCommon

	// For deferred (type-dependent) JSON parsing.
	Params json.RawMessage `json:"params"`
}

type config struct {
	Vars   map[string]interface{} `json:"vars"`
	Probes []*rawSpec             `json:"probes"`
}

// ProbeImpl is the probe-specific implementation interface.
type ProbeImpl interface {
	RunProbe(context.Context, *log.Logger) error
}

// Spec is the generic interface for probe implementation factory
// functions.
type Spec interface {
	Build(map[string]interface{}) (ProbeImpl, error)
}

type probeParserFunc func(json.RawMessage) (Spec, error)

var probeRegistry = make(map[string]probeParserFunc)

func RegisterProbeType(ptype string, pfunc probeParserFunc) {
	probeRegistry[ptype] = pfunc
}

// ParseConfig parses a JSON-encoded configuration and returns a list of
// Probe objects that can be executed or sent to the scheduler.
//
// Configuration parsing has a few phases: decoding the configuration
// itself is a relatively complex task, as we are implementing
// polymorphysm on probe definitions (via their "params"
// attribute).This requires a multi-level approach in Go, where JSON
// parsing is split into generic / specific stages, with the latter
// delegated to a type-specific parser accessed via a global
// registry. The result of this process is a "template" probe called a
// Spec, which has to be an interface type because the actual struct
// definition is private to the specific probe implementation.
//
// Then, parameterization is applied, potentially generating multiple
// Probes from each Spec. Variables are expanded, and all necessary
// information is bound together in the runtime-ready Probe object.
// The variable expansion process is reflect-heavy but is only
// performed once at the start of the process. The resulting Probe
// objects are minimal and lean.
//
func ParseConfig(data []byte, resultStore ResultStore) ([]*Probe, error) {
	var cfg config
	if err := json.Unmarshal(data, &cfg); err != nil {
		return nil, err
	}

	var out []*Probe
	for _, rawSpec := range cfg.Probes {
		probes, err := parseProbe(rawSpec, cfg.Vars, resultStore)
		if err != nil {
			return nil, err
		}
		out = append(out, probes...)
	}
	return out, nil
}

func parseProbe(rawSpec *rawSpec, baseVars map[string]interface{}, resultStore ResultStore) ([]*Probe, error) {
	// Basic sanity checks and defaults.
	if rawSpec.Type == "" {
		return nil, errors.New("attribute 'type' undefined")
	}
	if rawSpec.Name == "" {
		return nil, errors.New("attribute 'name' undefined")
	}
	if rawSpec.Interval.Duration == 0 {
		rawSpec.Interval = jsontypes.Duration{defaultInterval}
	}
	if rawSpec.Timeout.Duration == 0 {
		rawSpec.Timeout = jsontypes.Duration{defaultTimeout}
	}

	// Lookup the probe type in the registry, and finalize parsing
	// the type-specific part of the JSON spec using the factory
	// function.
	ptfn, ok := probeRegistry[rawSpec.Type]
	if !ok {
		return nil, fmt.Errorf("unknown probe type '%s'", rawSpec.Type)
	}
	spec, err := ptfn(rawSpec.Params)
	if err != nil {
		return nil, fmt.Errorf("error initializing %s probe: %v", rawSpec.Type, err)
	}

	// Generate all the different contexts if iterators are present.
	iterVars, err := vars.CrossProduct(rawSpec.Loop, baseVars)
	if err != nil {
		return nil, fmt.Errorf("error initializing %s probe: %v", rawSpec.Type, err)
	}

	// Build the probe implementation. This step can fail due to
	// syntax errors, missing parameters, etc.
	// Create a Probe runnable object that holds runtime information.
	var probes []*Probe
	for _, iv := range iterVars {
		// Obtain the factory (implementation).
		impl, err := spec.Build(iv)
		if err != nil {
			return nil, err
		}

		// Expand the variables in the spec.
		exp, err := vars.Expand(&rawSpec.SpecCommon, iv)
		if err != nil {
			return nil, err
		}

		probes = append(probes, &Probe{
			resultStore: resultStore,
			spec:        *(exp.(*SpecCommon)),
			vars:        iv,
			impl:        impl,
		})
	}

	return probes, checkUniqueness(probes)
}

func checkUniqueness(probes []*Probe) error {
	tmp := make(map[string]struct{})
	for _, p := range probes {
		name := p.Name()
		if _, ok := tmp[name]; ok {
			return fmt.Errorf("duplicate probe name '%s'", name)
		}
		tmp[name] = struct{}{}
	}
	return nil
}

// A Probe holds all the runtime information necessary to run a single
// probe, combining the SpecCommon parameters with a ProbeImpl to
// actually execute the probe.
type Probe struct {
	spec        SpecCommon
	impl        ProbeImpl
	resultStore ResultStore

	// for debugging
	vars map[string]interface{}
}

// Name is a convenience function to return the probe name.
func (p *Probe) Name() string { return p.spec.Name }

// Interval satisfies the scheduler.PeriodicEvent interface.
func (p *Probe) Interval() time.Duration { return p.spec.Interval.Duration }

const logFlags = log.Ldate | log.Ltime | log.Lmicroseconds

// Tick satisfies the scheduler.PeriodicEvent interface.
func (p *Probe) Tick(ctx context.Context) {
	ctx, cancel := context.WithTimeout(ctx, p.spec.Timeout.Duration)
	defer cancel()

	// Create a buffer to store the output (preallocate some space).
	logbuf := bytes.NewBuffer(make([]byte, 0, 4096))
	debug := log.New(logbuf, "", logFlags)

	// Allocate a new ID.
	var result Result
	result.ID = fmt.Sprintf("%08x", rand.Int63())
	result.Spec = p.spec
	result.Start = time.Now()

	log.Printf("starting probe %s", result.String())
	err := p.impl.RunProbe(ctx, debug)

	if err != nil {
		result.Error = err.Error()
		debug.Printf("ERROR: %v", err)
		log.Printf("probe %s failed: %v", result.String(), err)
	} else {
		result.Ok = true
		debug.Printf("probe succeeded")
		log.Printf("probe %s succeeded", result.String())
	}

	result.Duration = time.Since(result.Start)
	result.Logs = logbuf.String()

	// Store the result in the result store.
	p.resultStore.Push(&result)
}
